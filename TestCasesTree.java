import org.junit.Test;
import junit.framework.TestCase;

public class TestCasesTree extends TestCase{

@Test	
public void testInsertDelete1(){
    Tree <Integer> t = new Tree <Integer>();
    //assertEquals(true, t.insert(50));
    assertTrue(t.insert(50));
    assertTrue(t.insert(7));
    assertTrue(t.insert(11));
    assertTrue(t.insert(35));
    assertTrue(t.insert(50));
    assertTrue(t.insert(8));
    assertTrue(t.insert(61));
    assertTrue(t.insert(15));
    assertTrue(t.delete(8));
    assertTrue(t.delete(7));
    assertTrue(t.delete(11));
    assertTrue(t.delete(35));
  }

@Test	
public void testInsertDelete2(){
    Tree <Integer> t = new Tree <Integer>();
    //assertEquals(true, t.insert(50));
    assertTrue(t.insert(7));
    assertTrue(t.insert(87));
    assertTrue(t.insert(12));
    assertTrue(t.insert(34));
    assertTrue(t.insert(3));
    assertTrue(t.insert(1));
    assertTrue(t.insert(45));
    assertTrue(t.insert(5));
    
    assertEquals(false,t.delete(89));
    assertEquals(true, t.delete(3));
    assertEquals(false,t.delete(6));
    assertEquals(true, t.delete(45));
  }
}

