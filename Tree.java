

class Tree<T extends Comparable<T>>{
	private final static boolean DEBUG = true;
	private enum Color{RED, BLACK};
	private class Node {
		private int data;
		private Color color = Color.RED;
		private Node left = null, right = null, p = null;
		private T key = null;

		/**
    T must be a Comparable type
    Color is RED by default
		 */
		public Node(T key){ 
			this.key = key; 
		} 

		private boolean checkRoot(){
			return (p == null && color == Color.BLACK) || (p != null);
		}

		private int checkRBProperty(){
			if(right == null){
				if(left == null || left.checkRBProperty() == 1)
					return 1 + (color == Color.RED ? 0: 1);
				else
					return -1;
			}else if(left == null){
				if(right.checkRBProperty() == 1)
					return 1 + (color == Color.RED ? 0: 1);
				else
					return -1;
			}
			int l = left.checkRBProperty();
			int r = right.checkRBProperty();
			if(l == -1 || r == -1)
				return -1;

			if(l == r)
				return l + (color == Color.RED ? 0: 1);
			return -1;
		}
	}
	private Node root = null;

	public boolean insert(T k){
		Node z = new Node(k);
		Node y = null;
		Node x = root;
		System.out.println(z.key);
		while( x != null){
			y = x;
			if(z.key.compareTo(x.key) < 0)
				x = x.left;
			else
				x = x.right;
		}
		z.p = y;
		if(y == null)
			root = z;
		else if(z.key.compareTo(y.key) < 0)
			y.left = z;
		else
			y.right = z;

		insertFixup(z);
		if(DEBUG)
			return z.checkRBProperty() > 0;
			return true;
	}

	private void insertFixup(Node z){
		Node y;
		//comprobaremos las propiedades;
		if(z.p!=null && z.p.p!=null){
			//verificaremos si el padre es rojo sus hijos tienen que ser negros
			//este caso es cuando ingresamos por la izquierda
			if(z.p.color==Color.RED)
				if(z.p==z.p.p.left){
					y= z.p.p.right;
					//si "y" es hijo derecho y su padre es rojo  
					//entonces hacemos que sus hijos sean negros	
					if(y!=null && y.color==Color.RED ){ 
						z.p.color=Color.BLACK;
						y.color=Color.BLACK;
						z.p.p.color=Color.RED;
						z=z.p.p;
					}
					//haremos una rotacion de izquierda viendo que el nodo es hijo derecho
					else if(z==z.p.right){
						z=z.p;
						leftRotate(z);	
					}
					else{
						z.p.color=Color.BLACK;
						if(z.p.p!=null){
							z.p.p.color=Color.RED;
							rightRotate(z.p.p);
						}

					}
				}
		}
	}

	private void leftRotate (Node x){
		assert(x.right != null): "Right son was null"+ x.key;

		Node y = x.right;
		x.right = y.left;

		if(y.left != null)
			y.left.p = x;
		y.p = x.p;

		if(x.p == null)
			root = y;
		else if (x == x.p.left)
			x.p.left = y;
		else
			x.p.right = y;
		y.left = x;
	}

	private void rightRotate(Node y){
		assert(y.left!=null):"left son was null" + y.key;

		Node x=y.left;
		y.left=x.right;

		if(x.right!=null)
			x.right.p=y;
		x.p=y.p;
		if(y.p==null)
			root=x;
		else if(y==y.p.right)
			y.p.right=x;
		else
			y.p.left=x;
		x.right=y;
		y.p=x;

	}	

	public boolean delete(T z)
	{
		Node z = z.SearchKeyNode();
		Node y = z;
		Color ycolor= y.color;
		Node x = null;

		if(z.left == null && z.right == null)
		{
			if(z.p.left == z)
				z.p.left = null;
			else
				z.p.right = null;
		}
		else if(z.left == null)
		{
			x = z.right;
			transplant(z, z.right);
		}
		
		if(DEBUG)
			return z.checkRBProperty() > 0;
			return true;




	}
	private void transplant(Tree<T>.Node z, Tree<T>.Node right) {
		// TODO Auto-generated method stub
		
	}

	
}